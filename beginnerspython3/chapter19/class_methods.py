class Person:
    """
    Un empleado
    """
    instance_count = 0

    @classmethod
    def increment_instance_count(cls):
        cls.instance_count += 1

    def __init__(self, name, age):
        """
        Constructor
        :param name:    nombre del empleado
        :param age:     edad del empleado
        """
        Person.increment_instance_count()
        self.name = name
        self.age = age


p1 = Person('Jason', 36)
print(Person.instance_count)
p2 = Person('Carol', 21)
print(Person.instance_count)
Person.increment_instance_count()
print(Person.instance_count)
