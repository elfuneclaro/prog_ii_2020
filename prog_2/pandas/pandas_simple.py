import pandas as pd

complaints = pd.read_csv('data/311-service-requests.csv', dtype='unicode')

print(complaints['Complaint Type'])

print(complaints[:5])

print(complaints['Complaint Type'][:5])

print(complaints[['Complaint Type', 'Borough']])


data = {
    'apples': [3, 2, 0, 1],
    'oranges': [0, 3, 7, 2]
}

purchases = pd.DataFrame(data)

data = {
    'apples': [3, 2, 0, 1],
    'oranges': [0, 3, 7, 2]
}

purchases_indexed = pd.DataFrame(data, index=['June', 'Robert', 'Lily', 'David'])

print(purchases_indexed.loc['June'])