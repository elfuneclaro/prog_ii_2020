# https://docs.python.org/3/library/unittest.html

import unittest
from prog_2.unittest.calculator import Calculator

class TestCalculator(unittest.TestCase):

    def setUp(self):
        self.calculator = Calculator()

    def test_initial_value(self):
        self.assertEqual(self.calculator.total, 0)

    def test_add_one(self):
        self.calculator.set(1)
        self.calculator.add()
        self.assertEqual(self.calculator.total, 1)

    def test_subtract_one(self):
        self.calculator.set(1)
        self.calculator.sub()
        self.assertEqual(self.calculator.total, -1)

    def test_add_one_and_one(self):
        self.calculator.set(1)
        self.calculator.add()
        self.calculator.set(1)
        self.calculator.add()
        self.assertEqual(self.calculator.total, 2)

    def test_total(self):
        self.calculator.set(1)
        self.calculator.add()
        total = self.calculator.get_total()
        self.assertEqual(total, 1)
