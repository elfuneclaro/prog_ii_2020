def my_sum(*args):
    result = 0
    for x in args:
        result += x
    return result


my_sum(1, 2, 3)

list1 = [1, 2, 3]
list2 = [4, 5]
list3 = [6, 7, 8, 9]

print(my_sum(*list1))
print(my_sum(*list1, *list2, *list3))

my_first_dict = {"A": 1, "B": 2}
my_second_dict = {"C": 3, "D": 4}
my_merged_dict = {**my_first_dict, **my_second_dict}
print(my_merged_dict)


def my_function(*args, **kwargs):
    for arg in args:
        print('arg:', arg)
    for key in kwargs.keys():
        print('key:', key, 'has value: ', kwargs[key])


my_function('John', 'Denise', daughter='Phoebe', son='Adam')
print('-' * 50)
my_function('Paul', 'Fiona', son_number_one='Andrew', son_number_two='James', daughter='Joselyn')


def named(**kwargs):
    for key in kwargs.keys():
        print('arg:', key, 'has value:', kwargs[key])


named(a=1, b=2, c=3)


def printer(*args):
    for arg in args:
        print('arg:', arg, end=", ")
    print()


a = (1, 2, 3, 4)
b = [1, 2, 3, 4]

printer(0, 1, 2, 3, 4, 5)
printer(0, a, 5)
printer(0, b, 5)
printer(0, *a)
printer(0, *b)
printer(0, *[1, 2, 3, 4])

items = {"article_1": 1, "article_2": 2}


def sum_kg(dic_items: dict) -> float:
    """
    Sum of kg of items in dictionary

    :param dic_items:   item dictionary
    :return:            the sum in kg
    """
    result = 0
    for value in dic_items.values():
        result += value

    return result


print(sum_kg(items))

sum(items.values())


def expon(a, *args):
    return a ** sum(args)


print(expon(10, 2, 3, 1))


def print_kwargs(**kwargs):
    print(kwargs['a'])


print_kwargs(a=1, b=2)

student_tuples = [('john', 'A', 15),
                  ('jane', 'B', 12),
                  ('dave', 'B', 10)]

from typing import Tuple

def sorting_criteria(x: Tuple[str, str, int]) -> str:
    return x[0]

sorting_criteria(student_tuples[0])

sorted(student_tuples, key=sorting_criteria)

from math import exp

def g(f, x):
    return exp(f(x))

def f(x):
    return x**2

g(f, 3)

def g(f, x):
    return exp(f(x))

f = lambda x: x**2