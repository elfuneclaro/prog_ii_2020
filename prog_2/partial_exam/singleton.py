def singleton(cls):
    instance = None

    def get_instance():
        nonlocal instance
        if instance is None:
            instance = cls()
        return instance

    return get_instance


@singleton
class Service:
    def print_id(self):
        print(id(self))


s1 = Service()
s1.print_id()
s2 = Service()
s2.print_id()

# 'In singleton for: ', cls)