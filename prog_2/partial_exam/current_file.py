import os


class CurrentFile:

    def __get__(self, instance, owner=None):
        return sorted(os.listdir(instance.dirname),
                      key=os.path.getmtime, reverse=True)[0]


class Directory:
    current_file = CurrentFile()

    def __init__(self, dirname=os.getcwd()):
        self.dirname = dirname


c = Directory()
os.system('touch test.txt')
print(c.current_file)

# https://docs.python.org/3/library/os.path.html
# https://docs.python.org/3/library/os.html
