import functools
import os
import numpy as np
import tensorflow as tf
import pandas as pd
import re

"""
TRAIN_DATA_URL = "https://storage.googleapis.com/tf-datasets/titanic/train.csv"
TEST_DATA_URL = "https://storage.googleapis.com/tf-datasets/titanic/eval.csv"

train_file_path = tf.keras.utils.get_file("train.csv", TRAIN_DATA_URL)
test_file_path = tf.keras.utils.get_file("eval.csv", TEST_DATA_URL)
"""

PROJECT_PATH = '/Users/rlucerga/Dropbox (Personal)/Innitium/Clients/UFV/PII/06 ' \
               'Documents/hunt/beginnerspython3/neural_net'

# Data files
CSV_TRAIN_FILE = os.path.join(PROJECT_PATH, "train.csv")
CSV_TEST_FILE = os.path.join(PROJECT_PATH, "test_labeled.csv")

# Clean data files
CLEAN_TRAIN_PATH = os.path.join(PROJECT_PATH, 'train_clean.csv')
CLEAN_TEST_PATH = os.path.join(PROJECT_PATH, 'test_labeled_clean.csv')
OUTPUT_FILE = os.path.join(PROJECT_PATH, 'output.csv')

COLUMN_NAMES = ["passenger_id", "class", "name", "sex", "age", "n_siblings_spouses", "parch",
                "ticket", "fare", "cabin", "embark_town", "survived"]

MODEL_VARIABLES = ['sex', 'age', 'n_siblings_spouses', 'parch', 'fare', 'class',
                   'cabin', 'embark_town', 'survived']

# Make numpy values easier to read.
np.set_printoptions(precision=3, suppress=True)

LABEL_COLUMN = 'survived'
LABELS = [0, 1]


def clean_csv(file_path):
    data = pd.read_csv(file_path,
                       names=COLUMN_NAMES,
                       header=0,
                       usecols=["passenger_id"] + MODEL_VARIABLES,
                       na_values='',
                       dtype={
                           'class': str,
                           'cabin': str
                       })

    data['class'] = data['class'].apply(lambda x: 'Class {}'.format(x))

    # impute age with mean
    data["age"].fillna(data["age"].mean(), inplace=True)
    data["cabin"].fillna('unkown', inplace=True)
    data['cabin'] = data['cabin'].apply(lambda x: re.sub("([a-Z]).*", "\\1", str(x)))
    return data


# clean and write
train_clean = clean_csv(CSV_TRAIN_FILE)
train_clean.to_csv(CLEAN_TRAIN_PATH, sep=',', encoding='utf-8', index=False)

test_clean = clean_csv(CSV_TEST_FILE)
test_clean.to_csv(CLEAN_TEST_PATH, sep=',', encoding='utf-8', index=False)


def get_dataset(file_path):
    dataset = tf.data.experimental.make_csv_dataset(
        file_pattern=file_path,
        batch_size=5,  # Artificially small to make examples easier to show.
        label_name=LABEL_COLUMN,
        na_value="?",
        num_epochs=1,
        ignore_errors=True,
        shuffle=False)
    return dataset


raw_train_data = get_dataset(file_path=CLEAN_TRAIN_PATH)
raw_test_data = get_dataset(file_path=CLEAN_TEST_PATH)


def show_batch(dataset):
    for batch, label in dataset.take(1):
        for key, value in batch.items():
            print("{:20s}: {}".format(key, value.numpy()))


show_batch(raw_train_data)
show_batch(raw_test_data)

NUMERIC_FEATURES = ('age', 'n_siblings_spouses', 'parch', 'fare')


def pack_func(features, labels, names=NUMERIC_FEATURES):
    num_features = [features.pop(name) for name in names]
    num_features = [tf.cast(feat, tf.float32) for feat in num_features]
    num_features = tf.stack(num_features, axis=1)
    features['numeric'] = num_features
    return features, labels


packed_train_data = raw_train_data.map(pack_func)
show_batch(packed_train_data)
print('\n')
packed_test_data = raw_test_data.map(pack_func)
show_batch(packed_train_data)

example_batch, labels_batch = next(iter(packed_train_data))

desc = pd.read_csv(CLEAN_TRAIN_PATH)[list(NUMERIC_FEATURES)].describe()

MEAN = np.array(desc.T['mean'])
STD = np.array(desc.T['std'])


def normalize_numeric_data(data, mean, std):
    # Center the data
    return (data - mean) / std


# See what you just created.
normalizer = functools.partial(normalize_numeric_data, mean=MEAN, std=STD)

numeric_column = tf.feature_column.numeric_column('numeric', normalizer_fn=normalizer,
                                                  shape=[len(NUMERIC_FEATURES)])
numeric_columns = [numeric_column]
# numeric_column

# example_batch['numeric']

numeric_layer = tf.keras.layers.DenseFeatures(numeric_columns)
numeric_layer(example_batch).numpy()

CATEGORIES = {
    'sex': ['male', 'female'],
    'class': ['Class 1', 'Class 2', 'Class 3'],
    'cabin': ['a', 'B', 'C', 'D', 'E', 'F', 'G', 'T', 'unknown'],
    'embark_town': ['C', 'S', 'Q']
}

categorical_columns = []
for feature, vocab in CATEGORIES.items():
    cat_col = tf.feature_column.categorical_column_with_vocabulary_list(
        key=feature, vocabulary_list=vocab)
    categorical_columns.append(tf.feature_column.indicator_column(cat_col))

categorical_layer = tf.keras.layers.DenseFeatures(categorical_columns)
print(categorical_layer(example_batch).numpy()[0])

preprocessing_layer = tf.keras.layers.DenseFeatures(categorical_columns + numeric_columns)
print(preprocessing_layer(example_batch).numpy()[0])

model = tf.keras.Sequential([
    preprocessing_layer,
    tf.keras.layers.Dense(16, activation='relu'),
    tf.keras.layers.Dense(16, activation='relu'),
    tf.keras.layers.Dense(1),
])

model.compile(
    loss=tf.keras.losses.BinaryCrossentropy(from_logits=True),
    optimizer='adam',
    metrics=['accuracy'])

# train_data = packed_train_data.shuffle(500)
train_data = packed_train_data
test_data = packed_test_data

model.fit(train_data, epochs=20)

test_loss, test_accuracy = model.evaluate(test_data)

print('\n\nTest Loss {}, Test Accuracy {}'.format(test_loss, test_accuracy))

predictions = model.predict(test_data)
# predictions[:10]

# Show some results
for prediction, survived in zip(predictions[:10], list(test_data)[0][1][:10]):
    prediction = tf.sigmoid(prediction).numpy()
    print("Predicted survival: {:.2%}".format(prediction[0]),
          " | Actual outcome: ",
          ("SURVIVED" if bool(survived) else "DIED"))


def get_ids(dataset):
    ids = []
    for batch, label in dataset:
        ids.extend(batch.get("passenger_id").numpy())
    return ids


def output_df(data, file_path, threshold=0.30):
    pred = model.predict(data)

    results = {
        'passenger_id': get_ids(data),
        'survival_prob': tf.sigmoid(pred[:, 0]).numpy()
    }

    results = pd.DataFrame.from_dict(results)

    results["survival_pred"] = [(1 if i else 0) for i in results["survival_prob"] > threshold]

    output_set = pd.read_csv(file_path)

    output_set = output_set.set_index('passenger_id').join(results.set_index('passenger_id'))

    confusion_matrix = pd.crosstab(output_set['survived'],
                                   output_set['survival_pred'],
                                   rownames=['Actual'],
                                   colnames=['Predicted'])
    print(confusion_matrix)

    return output_set


output_df(train_data, CLEAN_TRAIN_PATH)
output = output_df(test_data, CLEAN_TEST_PATH)

output.to_csv(OUTPUT_FILE, sep=',', encoding='utf-8')
