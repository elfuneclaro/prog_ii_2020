"""
Interesting descriptors typically run computations instead of doing lookups:

Besides showing how descriptors can run computations, this example also reveals the purpose of
the parameters to __get__(). The self parameter is size, an instance of DirectorySize. The instanace
parameter is either g or s, an instance of Directory. It is instanace parameter that lets the __get__()
method learn the target directory. The owner parameter is the class Directory.

To run this properly, make sure your current workig directory is the repository
"""

# https://docs.python.org/3/howto/descriptor.html

import os


class DirectorySize:

    def __get__(self, obj, objtype=None):
        return len(os.listdir(obj.dirname))


class Directory:

    size = DirectorySize()              # Descriptor

    def __init__(self, dirname):
        self.dirname = dirname          # Regular instance attribute


g = Directory('beginnerspython3/chapter27')
s = Directory('beginnerspython3/chapter28')
print(g.size)                            # The games directory has three files

os.system('touch beginnerspython3/chapter27/newfile.txt')
# May work only in Mac/Linux, Add a fourth file to the directory
# Try type nul > chapter27/newfile.txt

print(g.size)

print(s.size)