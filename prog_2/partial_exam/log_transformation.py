from math import log


def log_tranform(natural=True):
    def wrap(func):
        def inner(x):
            if natural:
                return func(log(x) + 1)
            else:
                return func(log(x, 10) + 1)

        return inner

    return wrap


@log_tranform(natural=False)
def func1(x):
    return x ** 2 + 1


print(func1(3))


@log_tranform(natural=True)
def func1(x):
    return x ** 2 + 1


print(func1(3))

# When performing the data analysis, sometimes the data is skewed and not normal-distributed,
# and the data transformation is needed. We are very familiar with data
# transformation approaches such as log transformation, square root transformation.
# As a special case of logarithm transformation, log(x+1) or log(1+x) can also be used.


# (log(3, 10) + 1) ** 2 + 1 -> 3.1818872011445896 first
# (log(3) + 1) ** 2 + 1 - 5.404173538148803 second
# log(3 ** 2 + 1, 10) --> 1.0

