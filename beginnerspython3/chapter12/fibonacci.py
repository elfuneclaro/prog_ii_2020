# From https://www.geeksforgeeks.org/program-for-nth-fibonacci-number/
# Function for nth Fibonacci number


def fib_recursive(n):
    if n < 0:
        print("Incorrect input")
        # First Fibonacci number is 0
    elif n == 0:
        return 0
    # Second Fibonacci number is 1
    elif n == 1:
        return 1
    else:
        return fib_recursive(n - 1) + fib_recursive(n - 2)


# Time Complexity: T(n) = T(n-1) + T(n-2) which is exponential.

# Fibonacci Series using Dynamic Programming
def fib_dynamic(n):
    # Taking 1st two fibonacci nubers as 0 and 1
    fib_array = [0, 1]

    while len(fib_array) < n + 1:
        fib_array.append(0)

    if n <= 1:
        return n
    else:
        if fib_array[n - 1] == 0:
            fib_array[n - 1] = fib_dynamic(n - 1)

        if fib_array[n - 2] == 0:
            fib_array[n - 2] = fib_dynamic(n - 2)

    fib_array[n] = fib_array[n - 2] + fib_array[n - 1]
    return fib_array[n]


# Function for nth fibonacci number - Space Optimisataion
# Taking 1st two fibonacci numbers as 0 and 1

def fib_dynamic_2(n):
    a = 0
    b = 1
    if n < 0:
        print("Incorrect input")
    elif n == 0:
        return a
    elif n == 1:
        return b
    else:
        for i in range(2, n + 1):
            c = a + b
            a = b
            b = c
        return b


# Time
# Complexity: O(n)
# Extra
# Space: O(1)

# This code is contributed by Saket Modi

# Python 3 Program to find n'th fibonacci Number in
# with O(Log n) arithmatic operations

# Returns n'th fuibonacci number using table f[]
def fib_formula_1(n: int) -> int:
    MAX = 1000
    # Create an array for memoization
    f = [0] * MAX

    # Base cases
    if n == 0:
        return 0
    if n == 1 or n == 2:
        f[n] = 1
        return f[n]

        # If fib(n) is already computed
    if f[n]:
        return f[n]

    if n & 1:
        k = (n + 1) // 2
    else:
        k = n // 2

    # Applying above formula [Note value n&1 is 1
    # if n is odd, else 0.
    if n & 1:
        f[n] = (fib_formula_1(k) * fib_formula_1(k) + fib_formula_1(k - 1) * fib_formula_1(k - 1))
    else:
        f[n] = (2 * fib_formula_1(k - 1) + fib_formula_1(k)) * fib_formula_1(k)

    return f[n]


# This code is contributed by Nikita Tiwari.


def fib_formula_2(n: int) -> int:
    phi = (1 + 5 ** 0.5) / 2
    return round(phi ** n / 5 ** 0.5)


if __name__ == '__main__':
    import timeit
    print("fib_dynamic(35)")
    print(timeit.timeit("fib_formula_1(10)", setup="from __main__ import fib_formula_1"))
