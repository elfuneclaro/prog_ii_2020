# Numbers

1 # int
True # boolean
1e5 # float
complex(1,3) # complex

# Immutable sequences

# Strings
"This is a string"
result = 1
f"This is the result: {result}"

# Tupples
a = (2, 3)
b = ('A', 3, 5, 6)
b[0]
b[:3]

# Bytes
bytes([15, 15])
bytes('a', encoding='utf8')

# Mutable sequences

# Lists
my_list = [1, 2, 3]
my_list = [1, 'a', 3]
my_list = [(1, 2), (2, 3)]

# Bytearray
bytearray([15, 15])
bytearray('a', encoding='utf8')

# Sets

my_set = {'a', 'b', 'c'}
'a' in my_set
my_set_2 = {'b', 'c'}
my_set.union(my_set_2)
my_set_2.add('e')
my_set_2.remove('b')

fs = frozenset({'a', 'b', 'c'})
# fs.remove('b') fails

# Mappings

cities = {'Wales': 'Cardiff',
          'England': 'London',
          'Scotland': 'Edinburgh',
          'Northern Ireland': 'Belfast',
          'Ireland': 'Dublin'}

print(len(cities))
cities['Wales']
cities.get('Ireland')

print(cities.values())
print(cities.keys())
print(cities.items())

print('Wales' in cities)
print('France' not in cities)

# Callables

def print_msg():
    print('Hello World!')


print_msg()
print(type(print_msg))


def print_my_msg(msg):
    print(msg)


print_my_msg('Hello World')


def square(n):
    return n * n
# Store result from square in a variable
result = square(4)
print(result)

# Built in

# Square root calculation

import math
math.sqrt(4)

