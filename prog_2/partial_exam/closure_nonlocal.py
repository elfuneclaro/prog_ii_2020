# https://docs.python.org/3.3/library/inspect.html
import inspect


def f():
    a = 1

    def inner():
        nonlocal a
        print(a)
        a = 2

    return inner


g = f()
inspect.getclosurevars(g)
g()
inspect.getclosurevars(g)
g()
