class Department:
    """
    A department in the University, with name and code
    """

    def __init__(self, name: str, department_code: str):
        self.name = name
        self.department_code = department_code

        self.courses = {}

    def add_course(self,
                   course_description: str,
                   course_code: str,
                   n_credits: int) -> 'Course':
        """
        Adds a course to the deparment
        :param course_description:      description of the course
        :param course_code:             unique course code
        :param n_credits:               credits assigned to the course
        :return:                        the Course object created
        """
        course = Course(course_description, course_code, n_credits, self.department_code)
        self.courses[course.course_code] = course
        return self.courses[course.course_code]


class Course:
    """
    A course defined by a department
    """

    def __init__(self,
                 course_description: str,
                 course_code: str,
                 n_credits: int,
                 department_code: str):
        self.course_description = course_description
        self.course_code = course_code
        self.credits = n_credits
        self.department_code = department_code

        self.classes = set([])

    def add_class(self, year: int) -> 'CourseClass':
        """
        Adds a course class to a course
        :param year:    year of the course class
        :return:        the CourseClass object created
        """
        course_class = CourseClass(self, year)
        self.classes.add(course_class)
        return course_class


class CourseClass:
    """
    Each course will have a particular implementation in a given year, of this class CourseClass.
    The Courses need to be defined first by the departments
    Students enron in CourseClasses.
    CourseClass maintains a set with all the students enrolled in the course class.
    """

    def __init__(self, course: Course, year: int):
        self.course = course
        self.year = year
        self.students = set([])

    def add_student(self, student: 'Student'):
        """
        Adds a student to the class, adding him/her to the set of students
        :param student:     a Student object to be added to the course class
        :return:            None
        """
        self.students.add(student)


class Student:
    """
    A particular student, enroling in classes
    """

    def __init__(self, name: str, student_number: int):
        self.name = name
        self.student_number = student_number
        self.classes = set([])

    def enrol(self, course_class: CourseClass) -> None:
        """
        Enrol a student ot the class, adding him/her to the set of students of the course class
        and updating the set of class courses of the Student
        :param course_class:    class to enrol the student into
        :return:                None
        """
        self.classes.add(course_class)
        course_class.add_student(self)


maths_dept = Department(name="Mathematics and Applied Mathematics",
                        department_code="MAM")

mam1000w = maths_dept.add_course("Mathematics 1000", "MAM1000W", 1)
mam1000w_2013 = mam1000w.add_class(2013)

alg1000w = maths_dept.add_course("Algebra 1000", "ALG1000W", 1)
alg1000w_2013 = alg1000w.add_class(2013)

joe = Student("John Doe", 22030023)
joe.enrol(mam1000w_2013)
joe.enrol(alg1000w_2013)
print('end')

# Note forward references in type hinting
# Forward references https://www.python.org/dev/peps/pep-0484/
